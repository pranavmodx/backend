from django.apps import AppConfig


class CollectioncentresConfig(AppConfig):
    name = 'collectioncentres'
